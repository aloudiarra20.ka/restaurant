package com.app.restaurantgit.model;

public enum OrderStatus {
    COMMANDÉ,PAYÉ,COMPLÉTÉ,ANNULÉ
}
